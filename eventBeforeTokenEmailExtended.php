<?php
/**
 * eventBeforeTokenEmailExtended
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2017-2018 Denis Chenu <http://www.sondages.pro>
 * @license AGPL
 * @version 2.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class eventBeforeTokenEmailExtended extends PluginBase
{
    static protected $description = 'Extend beforeTokenEmail event';
    static protected $name = 'beforeTokenEmailExtended';

    private $PhpMailer;

    public function init()
    {
        $this->subscribe('afterPluginLoad');
        $this->subscribe('beforeTokenEmail');
    }

    /**
     * Unsure if we don't send ourself the email.
     * @link https://manual.limesurvey.org/BeforeTokenEmail
     */
    public function beforeTokenEmail()
    {
        global $maildebug;
        $oEvent=$this->event;

        $type=$oEvent->get('type'); /* todo : control attachement type */
        $iSurveyId=$oEvent->get("survey");
        $aToken=$oEvent->get("token");
        $oToken= $this->api->getTokenById($iSurveyId, $aToken['tid']);

        $newEvent = new PluginEvent('beforeTokenEmailExtended');
        $newEvent->set('survey', $oEvent->get('survey'));
        $newEvent->set('type', $oEvent->get('type'));
        $newEvent->set('model', $oEvent->get('model'));
        $newEvent->set('subject', $oEvent->get('subject'));
        $newEvent->set('to', $oEvent->get('to'));
        $newEvent->set('body', $oEvent->get('body'));
        $newEvent->set('from', $oEvent->get('from'));
        $newEvent->set('bounce', $oEvent->get('bounce'));
        $newEvent->set('token', $oEvent->get('token'));
        $newEvent->set('attachements',$this->getAttachementsLS($type,$oToken));
        App()->getPluginManager()->dispatchEvent($newEvent);

        $type=$newEvent->get('type');
        $subject = $newEvent->get('subject');
        $body = $newEvent->get('body');
        $aTo = (array) $newEvent->get('to');
        $sFrom = $newEvent->get('from');
        $sBounce = $newEvent->get('bounce');
        $sitename = $newEvent->get('sitename',Yii::app()->getConfig("sitename"));

        $iSurveyId=$oEvent->get("survey");
        $aToken=$newEvent->get("token");
        $oToken= $this->api->getTokenById($iSurveyId, $aToken['tid']);
        /* Get the php Mailer */
        $PhpMailer=$newEvent->get('PhpMailer',$this->setPhpMailer());
        /* Get the CustomHeaders */
        $pluginCustomHeaders=(array) $newEvent->get('CustomHeaders');
        /* is html */
        $bIsHtml=$newEvent->get("html",$oToken->survey->htmlemail=="Y");
        /* CC */
        $mailCC=$newEvent->get("addCC");
        /* BCC */
        $mailBCC=$newEvent->get("addBCC");
        /* Attachements */
        $aAttachements=(array) $newEvent->get("attachements");

        $emailcharset=Yii::app()->getConfig("emailcharset");
        /* non utf-8 */
        if($emailcharset!='utf-8') {
            $body=mb_convert_encoding($body,$emailcharset,'utf-8');
            $subject=mb_convert_encoding($subject,$emailcharset,'utf-8');
            $sitename=mb_convert_encoding($sitename,$emailcharset,'utf-8');
        }

        /* Fix from */
        $fromname='';
        $fromemail=$sFrom;
        if (strpos($sFrom,'<'))
        {
            $fromemail=substr($sFrom,strpos($sFrom,'<')+1,strpos($sFrom,'>')-1-strpos($sFrom,'<'));
            $fromname=trim(substr($sFrom,0, strpos($sFrom,'<')-1));
        }
        $PhpMailer->SetFrom($fromemail, $fromname);
        /* Fix sender */
        $sBounce=empty($sBounce) ? $fromemail : $sBounce;
        $PhpMailer->Sender = $sBounce;
        foreach ($aTo as $singletoemail)
        {
            if (strpos($singletoemail, '<') )
            {
                $toemail=substr($singletoemail,strpos($singletoemail,'<')+1,strpos($singletoemail,'>')-1-strpos($singletoemail,'<'));
                $toname=trim(substr($singletoemail,0, strpos($singletoemail,'<')-1));
                $PhpMailer->AddAddress($toemail,$toname);
            }
            else
            {
                $PhpMailer->AddAddress($singletoemail);
            }
        }

        $PhpMailer->Subject=$subject;
        /* Headers */
        $aCustomHeaders=array_merge(
            array(
                'X-Surveymailer'=>"$sitename Emailer (LimeSurvey Extended)",
            ),
            $pluginCustomHeaders,
            array(
                'X-surveyid'=>$oToken->survey->sid,
                'X-tokenid'=>$oToken->token,
            )
        );
        foreach($aCustomHeaders as $key=>$value) {
            if(is_int($key) || ctype_digit($key)){ /* @todo : test if it's OK */
                $PhpMailer->AddCustomHeader($value); /* Control validity ? */
            } else {
                $PhpMailer->AddCustomHeader($key,$value);
            }
        }

        /* Body */
        /* sometimes, must fix */
        if (get_magic_quotes_gpc() != "0") {
            $body = stripcslashes($body);
        }
        if($bIsHtml) {
            $PhpMailer->IsHTML(true);
            if(strpos($body,"<html>")===false)
            {
                $body="<html>".$body."</html>";
            }
            require_once(APPPATH.'/third_party/html2text/src/Html2Text.php');
            $html=new \Html2Text\Html2Text($body);
            $PhpMailer->AltBody=$html->getText();
            $PhpMailer->msgHTML($body,App()->getConfig("publicdir")); // This allow embedded image if we remove the servername from image
        } else {
            $PhpMailer->IsHTML(false);
            $PhpMailer->Body = $body;
        }
        /* CC and Bcc */
        $this->addRecipients('CC',$mailCC);
        $this->addRecipients('BCC',$mailBCC);
        /* Attachment */
        $this->addAttachements($aAttachements);
        /* Finally send the email */
        $emailsmtpdebug=Yii::app()->getConfig("emailsmtpdebug");
        if ($emailsmtpdebug>0) {
            ob_start();
        }
        $sent=$PhpMailer->Send();
        $errors=$PhpMailer->ErrorInfo;
        $debugging=ob_get_contents();
        if (ob_get_length()) {
            ob_end_clean();
        }

        $oEvent->set('send',false);
        if($sent) {
            $maildebug = gT('SMTP debug output:').'<pre>'.htmlentities($debugging).'</pre>';
            return;
        }
        if(Yii::app()->getConfig("emailsmtpdebug")) {
            /* In case of error :! limesurvey force html entities , always ... LS issue to fix */
            /* https://github.com/LimeSurvey/LimeSurvey/pull/677 started */
            $oEvent->set('error',$errors."\n".$debugging);
        } else {
            $oEvent->set('error',$errors);
        }
    }

    /**
     * Alter Yii : Put bounce in autoLoader
     * @see event afterPluginLoad
     */
    public function afterPluginLoad(){
        /* @todo : move all email function to a clean Class */
        Yii::setPathOfAlias('sendMail', dirname(__FILE__)."/");
    }

    /**
     * Create andset the base for PhpMailer
     * @return PHPMailer Class
     */
    public function setPhpMailer() {
        if($this->PhpMailer) {
            /* Must reset partially */
            $this->PhpMailer->clearAddresses();
            $this->PhpMailer->clearCustomHeaders();
            $this->PhpMailer->clearAttachments();
            return $this->PhpMailer;
        }
        /* Calling PHPMailer change in 3.4.1 */
        if(is_file(APPPATH.'/third_party/phpmailer/PHPMailerAutoload.php')) {
            require_once(APPPATH.'/third_party/phpmailer/PHPMailerAutoload.php');
            $PhpMailer = new PHPMailer;
        } else {
            require_once(APPPATH.'/third_party/phpmailer/load_phpmailer.php');
            $PhpMailer = new PHPMailer\PHPMailer\PHPMailer;
        }

        /* language and charset */
        if (!$PhpMailer->SetLanguage(Yii::app()->getConfig("defaultlang"),APPPATH.'/third_party/phpmailer/language/'))
        {
            $PhpMailer->SetLanguage('en',APPPATH.'/third_party/phpmailer/language/');
        }
        $PhpMailer->CharSet = Yii::app()->getConfig("emailcharset");
        /* SSL/TLS */
        $PhpMailer->SMTPAutoTLS=false;
        $emailsmtpssl = Yii::app()->getConfig("emailsmtpssl");
        if (isset($emailsmtpssl) && trim($emailsmtpssl)!=='' && $emailsmtpssl!==0) {
            if ($emailsmtpssl===1) {
                $PhpMailer->SMTPSecure = "ssl";
            } else {
                $PhpMailer->SMTPSecure = $emailsmtpssl;
            }
        }
        /* method */
        $emailmethod = Yii::app()->getConfig("emailmethod");
        $emailsmtphost = Yii::app()->getConfig("emailsmtphost");
        $emailsmtpuser = Yii::app()->getConfig("emailsmtpuser");
        $emailsmtppassword = Yii::app()->getConfig("emailsmtppassword");
        $emailsmtpdebug=Yii::app()->getConfig("emailsmtpdebug");
        switch ($emailmethod) {
            case "qmail":
                $mail->IsQmail();
                break;
            case "smtp":
                $PhpMailer->IsSMTP();
                if ($emailsmtpdebug>0)
                {
                    $PhpMailer->SMTPDebug = $emailsmtpdebug;
                }
                if (strpos($emailsmtphost,':')>0)
                {
                    $PhpMailer->Host = substr($emailsmtphost,0,strpos($emailsmtphost,':'));
                    $PhpMailer->Port = substr($emailsmtphost,strpos($emailsmtphost,':')+1);
                }
                else {
                    $PhpMailer->Host = $emailsmtphost;
                }
                $PhpMailer->Username =$emailsmtpuser;
                $PhpMailer->Password =$emailsmtppassword;
                if (trim($emailsmtpuser)!="")
                {
                    $PhpMailer->SMTPAuth = true;
                }
                break;
            case "sendmail":
                $PhpMailer->IsSendmail();
                break;
            default:
                //Set to the default value to rule out incorrect settings.
                $emailmethod="mail";
                $PhpMailer->IsMail();
        }
        $this->PhpMailer=$PhpMailer;
        return $this->PhpMailer;
    }

    /**
     * Add the attchement
     * @param string $type
     * @param Token @oToken
     * @return void
     */
    public function getAttachementsLS($type,$oToken)
    {
        /* Use a static ? for optimisation ? More memory but less request */
        /* if yes : unserialize($oSurveyAttachement->attachments); go in the static array */
        $aRelevantAttachments = array();
        $sLanguage=$oToken->language;
        $iSurveyId=$oToken->survey->sid;
        $oSurveyAttachement=SurveyLanguageSetting::model()->find(array(
            'select'=>'attachments',
            'condition'=>'surveyls_survey_id=:sid and surveyls_language=:language',
            'params'=>array(':sid'=>$iSurveyId,':language'=>$sLanguage)
        ));
        if ($oSurveyAttachement && $oSurveyAttachement->attachments) {
            $aAttachments = unserialize($oSurveyAttachement->attachments);
            if (!empty($aAttachments)) {
                if (isset($aAttachments[$type])) {
                    LimeExpressionManager::singleton()->loadTokenInformation($oToken->survey->sid, $oToken->token);
                    foreach ($aAttachments[$type] as $aAttachment) {
                        if (LimeExpressionManager::singleton()->ProcessRelevance($aAttachment['relevance'])) {
                            $aRelevantAttachments=$aAttachment['url'];
                        }
                    }
                }
            }
        }
        return $aRelevantAttachments;
    }

    /**
     * Add the attchement
     * @param array $aAttachements
     * @return void
     */
    public function addAttachements($aAttachements)
    {
        foreach($aAttachements as $aAttachement) {
            /* send a string */
            if(!is_array($aAttachement)) {
                $aAttachement=array(
                    'path'=>$aAttachement
                );
            }
            /* send an array path,name */
            if(!isset($aAttachement['path'])) {
                $aAttachement=array(
                    'path'=>$aAttachement[0],
                    'name'=>$aAttachement[1],
                );
            }
            $aAttachement=array_merge(
                array('path'=>'','name'=>'','encoding'=>'base64','type'=>'','disposition'=>'attachment'),
                $aAttachement
            );
            $added=$this->PhpMailer->addAttachment(
                $aAttachement['path'],
                $aAttachement['name'],
                $aAttachement['encoding'],
                $aAttachement['type'],
                $aAttachement['disposition']
            );
        }
    }
    /**
     * add the CC
     * @param string $stype (CC|BCC) (other to add)
     * @param null|string[]
     * @return void
     */
    public function addRecipients($type,$aMails=null) {
        if(empty($aMails)) {
            return;
        }
        foreach($aMails as $aMail) {
            $addMails= preg_split( "/(,|;)/", $aMail);
            foreach($addMails as $addMail) {
                /* Get adress with user name/email too : by array and by <> ? */
                if(validateEmailAddress($addMail)) { /* Or use PHPMailer->validateAddress() ? */
                    switch($stype) {
                        case 'CC':
                            $this->PhpMailer->addCC($addMail);
                            break;
                        case 'BCC':
                            $this->PhpMailer->addBCC($addMail);
                            break;
                        default:
                            // Throw error
                    }
                }
            }
        }
    }

}
